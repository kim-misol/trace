# trace_demo

## Requirements

Python 3.9+

```angular2html
python -m venv venv # venv 이름으로 가상환경 생성
source venv/bin/activate
pip install -r requirements.txt
```

## DB 설정

GRANT to root user

```bash
:~$ psql postgres
psql (14.1)
Type "help" for help.

postgres=# CREATE DATABASE trace;
CREATE DATABASE
postgres=# GRANT ALL PRIVILEGES ON DATABASE trace to root;
GRANT
postgres=#
```

## Development Server

```
$ cd mysite
$ python manage.py migrate
$ python manage.py runserver
$ python manage.py makemigrations algorithms  # settings.py INSTALLED_APPS 에 추가 후 장고에게 새로운 모델을 만들었다는 것을 알려준

```

## Frontend

```
$ cd frontend
$ npm start
```

## Smart Contract 
in our project directory (trace):
```
$ truffle init
```

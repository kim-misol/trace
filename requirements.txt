asgiref==3.4.1
certifi==2021.10.8
cffi==1.15.0
charset-normalizer==2.0.10
cryptography==36.0.1
Django==4.0.1
django-rest-knox==4.1.0
djangorestframework==3.13.1
greenlet==1.1.2
idna==3.3
numpy==1.22.1
pandas==1.3.5
plotly==5.5.0
psycopg2==2.9.3
pycparser==2.21
python-dateutil==2.8.2
pytz==2021.3
requests==2.27.1
six==1.16.0
SQLAlchemy==1.4.29
sqlparse==0.4.2
tenacity==8.0.1
urllib3==1.26.8
